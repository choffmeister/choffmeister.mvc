﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Choffmeister.Mvc.Helpers
{
    public class FlashMessages
    {
        private List<string> _infos;
        private List<string> _successes;
        private List<string> _warnings;
        private List<string> _errors;

        public bool HasInfos
        {
            get { return _infos.Count > 0; }
        }

        public bool HasSuccesses
        {
            get { return _successes.Count > 0; }
        }

        public bool HasWarnings
        {
            get { return _warnings.Count > 0; }
        }

        public bool HasErrors
        {
            get { return _errors.Count > 0; }
        }

        public IEnumerable<string> Infos
        {
            get { return _infos; }
        }

        public IEnumerable<string> Successes
        {
            get { return _successes; }
        }

        public IEnumerable<string> Warnings
        {
            get { return _warnings; }
        }

        public IEnumerable<string> Errors
        {
            get { return _errors; }
        }

        public FlashMessages(TempDataDictionary tempDataDirectory)
        {
            _infos = tempDataDirectory.ContainsKey("flash_info") ? (List<string>)tempDataDirectory["flash_info"] : new List<string>();
            _successes = tempDataDirectory.ContainsKey("flash_success") ? (List<string>)tempDataDirectory["flash_success"] : new List<string>();
            _warnings = tempDataDirectory.ContainsKey("flash_warning") ? (List<string>)tempDataDirectory["flash_warning"] : new List<string>();
            _errors = tempDataDirectory.ContainsKey("flash_error") ? (List<string>)tempDataDirectory["flash_error"] : new List<string>();
        }
    }
}