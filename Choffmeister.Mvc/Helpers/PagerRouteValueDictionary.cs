﻿using System.Collections.Specialized;
using System.Linq;
using System.Web.Routing;

namespace Choffmeister.Mvc.Helpers
{
    public class PagerRouteValueDictionary : RouteValueDictionary
    {
        public PagerRouteValueDictionary(NameValueCollection nvc, int page)
        {
            foreach (string key in nvc.AllKeys.Where(n => n != "page"))
            {
                this.Add(key, nvc[key]);
            }

            this.Add("page", page);
        }
    }
}