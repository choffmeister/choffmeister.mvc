﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Choffmeister.Mvc.Helpers
{
    public class Pager<TModel> : IPageable<TModel>
    {
        private readonly object _lock = new object();
        private readonly IEnumerable<TModel> _enumeration;
        private readonly Func<int> _countDelegate;

        private List<TModel> _items;
        private int _totalItemCount;
        private int _currentPage;
        private int _pageSize;

        public int TotalItemCount
        {
            get
            {
                if (_items == null)
                    this.Execute();

                return _totalItemCount;
            }
        }

        public int TotalPageCount
        {
            get { return this.TotalItemCount % _pageSize == 0 ? this.TotalItemCount / _pageSize : this.TotalItemCount / _pageSize + 1; }
        }

        public int CurrentPage
        {
            get { return _currentPage; }
        }

        public int PageSize
        {
            get { return _pageSize; }
        }

        public bool IsFirstPage
        {
            get { return _currentPage == 0; }
        }

        public bool IsLastPage
        {
            get { return _currentPage == this.TotalPageCount - 1; }
        }

        public Pager(IEnumerable<TModel> enumeration, int currentPage = 0, int pageSize = 20)
            : this(enumeration, null, currentPage, pageSize)
        {
        }

        public Pager(IEnumerable<TModel> enumeration, Func<int> countDelegate, int currentPage = 0, int pageSize = 20)
        {
            if (enumeration == null)
                throw new ArgumentNullException("enumeration");
            if (currentPage < 0)
                throw new ArgumentOutOfRangeException("currentPage");
            if (pageSize < 1)
                throw new ArgumentOutOfRangeException("pageSize");

            _enumeration = enumeration;
            _countDelegate = countDelegate;
            _currentPage = currentPage;
            _pageSize = pageSize;
            _items = null;
        }

        public Pager<TModel> Execute()
        {
            lock (_lock)
            {
                if (_items == null)
                {
                    _totalItemCount = _countDelegate != null ? _countDelegate() : _enumeration.Count();
                    _items = _enumeration.Skip(_currentPage * _pageSize).Take(_pageSize).ToList();
                }
            }
            return this;
        }

        public IEnumerable<int> GetSurroundingPageIndices(int range = 2)
        {
            if (range < 0)
                throw new ArgumentOutOfRangeException("range");

            if (this.TotalPageCount <= range * 2 + 1)
            {
                return Enumerable.Range(0, this.TotalPageCount);
            }
            else if (this.CurrentPage <= range)
            {
                return Enumerable.Range(0, range * 2 + 1);
            }
            else if (this.CurrentPage >= this.TotalPageCount - range)
            {
                return Enumerable.Range(this.TotalPageCount - range * 2 - 1, range * 2 + 1);
            }
            else
            {
                return Enumerable.Range(this.CurrentPage - range, range * 2 + 1);
            }
        }

        public IEnumerator<TModel> GetEnumerator()
        {
            if (_items == null)
                this.Execute();

            return _items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}