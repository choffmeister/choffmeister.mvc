﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Choffmeister.Mvc.Helpers
{
    public static class FlashHelper
    {
        public static void FlashInfo(this Controller controller, string message)
        {
            AppendTempData(controller.TempData, "flash_info", message);
        }

        public static void FlashSuccess(this Controller controller, string message)
        {
            AppendTempData(controller.TempData, "flash_success", message);
        }

        public static void FlashWarning(this Controller controller, string message)
        {
            AppendTempData(controller.TempData, "flash_warning", message);
        }

        public static void FlashError(this Controller controller, string message)
        {
            AppendTempData(controller.TempData, "flash_error", message);
        }

        private static void AppendTempData(TempDataDictionary dictionary, string key, string text)
        {
            List<string> list;

            if (!dictionary.ContainsKey(key))
            {
                list = new List<string>();
                dictionary[key] = list;
            }
            else
            {
                list = (List<string>)dictionary[key];
            }

            list.Add(text);
        }

        public static FlashMessages GetFlashes(this HtmlHelper helper)
        {
            return new FlashMessages(helper.ViewContext.TempData);
        }

        public static MvcHtmlString Flashes(this HtmlHelper helper, string partialViewName = "_Flashes")
        {
            return helper.Partial(partialViewName, GetFlashes(helper));
        }

        public static void RenderFlashes(this HtmlHelper helper, string partialViewName = "_Flashes")
        {
            helper.RenderPartial(partialViewName, GetFlashes(helper));
        }
    }
}