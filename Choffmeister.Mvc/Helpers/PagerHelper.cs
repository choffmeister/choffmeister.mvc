﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Choffmeister.Mvc.Helpers
{
    public static class PagerHelper
    {
        public static Pager<TModel> AsPageable<TModel>(this IEnumerable<TModel> enumeration,
            int currentPage = 0, int pageSize = 20)
        {
            return new Pager<TModel>(enumeration, currentPage, pageSize);
        }

        public static Pager<TModel> AsPageable<TModel>(this IEnumerable<TModel> enumeration, Func<int> countDelegate,
            int currentPage = 0, int pageSize = 20)
        {
            return new Pager<TModel>(enumeration, countDelegate, currentPage, pageSize);
        }

        public static MvcHtmlString Pager(this HtmlHelper helper, IPageable pageable, string partialViewName = "_Pager")
        {
            return helper.Partial(partialViewName, pageable);
        }

        public static void RenderPager(this HtmlHelper helper, IPageable pageable, string partialViewName = "_Pager")
        {
            helper.RenderPartial(partialViewName, pageable);
        }
    }
}