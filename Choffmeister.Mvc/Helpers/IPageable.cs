﻿using System.Collections;
using System.Collections.Generic;

namespace Choffmeister.Mvc.Helpers
{
    public interface IPageable<TModel> : IPageable, IEnumerable<TModel>
    {
    }

    public interface IPageable : IEnumerable
    {
        int TotalItemCount { get; }

        int TotalPageCount { get; }

        int CurrentPage { get; }

        int PageSize { get; }

        bool IsFirstPage { get; }

        bool IsLastPage { get; }

        IEnumerable<int> GetSurroundingPageIndices(int range = 2);
    }
}